﻿namespace Shop.Models.Models.Domain
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}