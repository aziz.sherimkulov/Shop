﻿using Microsoft.AspNetCore.Identity;
using Shop.Models.Models.Domain;

namespace Shop.Models.Models.Role
{
    public class Role : IdentityRole<int>, IEntity
    {
        public override string Name { get; set; }
    }
}