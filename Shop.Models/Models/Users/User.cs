﻿using Microsoft.AspNetCore.Identity;
using Shop.Models.Models.Domain;

namespace Shop.Models.Models.Users
{
    public class User : IdentityUser<int>, IEntity
    {
        public string Name { get; set; }

        public override string Email { get; set; }  
    }
}