﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shop.DAL.Context;
using Shop.Models.Models.Role;
using Shop.Models.Models.Users;

namespace Shop.DAL.Seed
{
    public static class DefaultSeed
    {
        private const string AdminName = "Admin";

        private const string AdminPassword = "123qwe!@#QWE";

        private const string MailConf = "@gmail.com";    

        public static async Task Seed(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            await SeedRoles(roleManager);
            await SeedUsers(userManager);
        }
        
        private static async Task SeedRoles(RoleManager<Role> roleManager)
        {
            if (roleManager.FindByNameAsync("Admin") == null)
                await roleManager.CreateAsync(new Role {Name = AdminName});
        }
        
        private static async Task SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByEmailAsync("admin@gmail.com") == null)
            {
                var user = new User{Name = AdminName, Email = AdminName + MailConf};

                IdentityResult result = await userManager.CreateAsync(user, AdminPassword);

                if (result.Succeeded) await userManager.AddToRoleAsync(user, AdminName);
            }
        }
    }
}