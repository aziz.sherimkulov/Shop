﻿using Shop.DAL.ContextFactory;

namespace Shop.DAL.UnitOfWorkFactory
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _contextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public UnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork(_contextFactory.CreateNewDbContext());
        }
    }
}