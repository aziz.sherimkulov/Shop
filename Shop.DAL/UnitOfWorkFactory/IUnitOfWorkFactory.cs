﻿namespace Shop.DAL.UnitOfWorkFactory
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork CreateUnitOfWork();    
    }
}