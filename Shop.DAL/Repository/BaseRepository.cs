﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shop.DAL.Context;
using Shop.Models.Models.Domain;

namespace Shop.DAL.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<T> _dbSet;

        public BaseRepository(
            ApplicationDbContext context
            )
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public async Task<int> CreateAsync(T entity)
        {
            var entityEntry = await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entityEntry.Entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(T entity)
        {
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<List<T>> GetAllAsync() => await _dbSet.ToListAsync();

        public async Task GetByIdAsync(int id) => await _dbSet.FindAsync(id);
    }
}