﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Shop.Models.Models.Domain;

namespace Shop.DAL.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        Task<int> CreateAsync(T entity);

        Task UpdateAsync(T entity);

        Task RemoveAsync(T entity);

        Task<List<T>> GetAllAsync();

        Task GetByIdAsync(int id);    
    }
}