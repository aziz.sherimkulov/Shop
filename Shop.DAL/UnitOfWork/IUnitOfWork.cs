﻿using System;
using System.Threading.Tasks;
using Shop.DAL.Repository;
using Shop.Models.Models.Domain;

namespace Shop.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> CompleteAsync();
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;
    }
}