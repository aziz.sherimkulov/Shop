﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Shop.DAL.Context;
using Shop.DAL.Repository;
using Shop.Models.Models.Domain;

namespace Shop.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;
        
        public UnitOfWork(ApplicationDbContext context)    
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }
        
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new BaseRepository<TEntity>(_context)) as IRepository<TEntity>;
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}