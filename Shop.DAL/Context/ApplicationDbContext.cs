﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Shop.Models.Models.Role;
using Shop.Models.Models.Users;

namespace Shop.DAL.Context
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}