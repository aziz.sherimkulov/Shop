﻿using Microsoft.EntityFrameworkCore;
using Shop.DAL.Context;

namespace Shop.DAL.ContextFactory
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions _options;

        public ApplicationDbContextFactory(DbContextOptions options)
        {
            _options = options;
        }

        public ApplicationDbContext CreateNewDbContext()
        {
            return new ApplicationDbContext(_options);
        }
    }
}