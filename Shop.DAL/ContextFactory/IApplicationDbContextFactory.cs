﻿using Shop.DAL.Context;

namespace Shop.DAL.ContextFactory
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateNewDbContext();
    }
}