using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shop.DAL.Context;
using Shop.DAL.ContextFactory;
using Shop.DAL.Seed;
using Shop.DAL.UnitOfWorkFactory;
using Shop.Models.Models.Role;
using Shop.Models.Models.Users;

namespace Shop.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add connection string
            services.AddDbContext<ApplicationDbContext>(
                option => 
                    option.UseSqlServer
                        (Configuration.GetConnectionString("DefaultConnection")));
            // Add DbContext and UnitOfWork    
            var optionBuilder = new DbContextOptionsBuilder();

            services.AddSingleton<IApplicationDbContextFactory>(
                sp =>
                    new ApplicationDbContextFactory(optionBuilder.Options));

            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();
            // Add Identity
            services.AddIdentity<User, Role>(
                    options =>
                        options.SignIn.RequireConfirmedAccount = false)
                .AddDefaultTokenProviders()
                .AddDefaultUI()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            
            // Add AutoMapper
            services.AddAutoMapper(typeof(MapperConfiguration));
            
            // Add Razor pages
            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            DefaultSeed.Seed(roleManager, userManager);
        }
    }
}
